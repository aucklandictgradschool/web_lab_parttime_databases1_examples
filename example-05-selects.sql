-- Drop existing tables (to get rid of any "old" data)
------------------------------------------------------------------------
drop table if exists attends;
drop table if exists teaches;
drop table if exists courses;
drop table if exists students;
drop table if exists lecturers;
------------------------------------------------------------------------


-- Create schema
------------------------------------------------------------------------
create table students (
    id int not null,
    fname varchar(20),
    lname varchar(100),
    country char(2),
    primary key (id),
    check (country like '__')
);

create table lecturers (
    staff_no int not null,
    fname varchar(32),
    lname varchar(32),
    office char(8),
    primary key (staff_no)
);

create table courses (
    dept varchar(10) not null,
    num int not null,
    description text,
    coord_no int,
    rep_id int,
    primary key(dept, num),
    foreign key (coord_no) references lecturers (staff_no),
    foreign key (rep_id) references students (id)
);

create table attends (
    student_id int not null,
    course_dept varchar(10) not null,
    course_num int not null,
    semester char(2),
    mark char(2),
    primary key (student_id, course_dept, course_num),
    foreign key (student_id) references students (id),
    foreign key (course_dept, course_num) references courses (dept, num)
);

create table teaches (
    course_dept varchar(10) not null,
    course_num int not null,
    staff_no int not null,
    semester char(2) not null,
    primary key (course_dept, course_num, staff_no, semester),
    foreign key (course_dept, course_num) references courses (dept, num),
    foreign key (staff_no) references lecturers (staff_no)
);
------------------------------------------------------------------------


-- Add data to database
------------------------------------------------------------------------
insert into students values (1661, 'James', 'Beam', 'US');
insert into students values (1668, 'Jack', 'Daniels', 'US');
insert into students values (1713, 'James', 'Speight', 'NZ');
insert into students values (1715, 'Henry', 'Wagstaff', 'NZ');
insert into students values (1717, 'Johnnie', 'Walker', 'AU');
insert into students values (1824, 'Tia', 'Maria', 'MX');
insert into students values (1828, 'Dom', 'Perignon', 'FR');
insert into students values (1970, 'Scarlett', 'Ohara', 'SC');
insert into students values (1971, 'Rob', 'Roy', 'SC');


insert into lecturers values (666, 'Bob', 'Marley', '302.180');
insert into lecturers values (707, 'Lady', 'Gaga', '303S.G75');
insert into lecturers values (123, 'Taylor', 'Swift', '720.487');
insert into lecturers values (456, 'Freddie', 'Mercury', '102.210');
insert into lecturers values (878, 'Marshall', 'Mathers', '302.865');
insert into lecturers values (919, 'David', 'Bowie', '123.456');
insert into lecturers values (1234, 'Alanis', 'Morissette', '303.520');
insert into lecturers values (5678, 'Daft', 'Punk', '303.516');

insert into courses values ('COMPSCI', 718, 'Java course', 5678, 1661);
insert into courses values ('COMPSCI', 719, 'Web course', 5678, 1970);
insert into courses values ('COMPSCI', 101, 'Principles of Programming', 666, 1971);
insert into courses values ('JAPANESE', 130, 'Japanese 1A', 123, 1668);
insert into courses values ('JAPANESE', 131, 'Japanese 1B', 919, 1970);
insert into courses values ('LINGUIST', 101, 'Language, Mind and Society', 878, 1824);

insert into attends values (1661, 'COMPSCI', 718, 'S1', 'A');
insert into attends values (1668, 'COMPSCI', 718, 'S1', 'A+');
insert into attends values (1717, 'COMPSCI', 718, 'S1', 'B');
insert into attends values (1970, 'COMPSCI', 719, 'S2', 'C+');
insert into attends values (1971, 'COMPSCI', 101, 'S1', 'A-');
insert into attends values (1824, 'LINGUIST', 101, 'S1','A');
insert into attends values (1661, 'JAPANESE', 130, 'S1','D');
insert into attends values (1668, 'JAPANESE', 130, 'S1', 'A+');
insert into attends values (1970, 'JAPANESE', 131, 'S2', 'A-');

insert into teaches values ('COMPSCI', 718, 5678, 'S1');
insert into teaches values ('COMPSCI', 718, 1234, 'S2');
insert into teaches values ('COMPSCI', 719, 5678, 'S1');
insert into teaches values ('COMPSCI', 719, 1234, 'S2');
insert into teaches values ('COMPSCI', 101, 666, 'S1');
insert into teaches values ('COMPSCI', 101, 707, 'S1');
insert into teaches values ('JAPANESE', 130, 123, 'S1');
insert into teaches values ('JAPANESE', 130, 456, 'S1');
insert into teaches values ('JAPANESE', 131, 123, 'S2');
insert into teaches values ('JAPANESE', 131, 919, 'S2');
insert into teaches values ('LINGUIST', 101, 878, 'S1');
insert into teaches values ('LINGUIST', 101, 123, 'S1');
------------------------------------------------------------------------


-- Querying the data
------------------------------------------------------------------------

-- I want to get a list of all of the students. I want all data in the students table.
select *
from students;

-- I want to get the student id's and first names of all students.
select id, fname
from students;

-- I want to get all students from NZ.
select *
from students
where country = 'NZ';

-- I want to get all lecturers in building 303
select *
from lecturers
where office like '303.%';

-- I want to get the department and code of all courses taught in semester one.
select distinct course_dept, course_num -- "distinct" removes duplicate values in the result.
from teaches
where semester = 'S1';
------------------------------------------------------------------------