-- Drop existing tables (to get rid of any "old" data)
------------------------------------------------------------------------
drop table if exists students;
drop table if exists lecturers;
drop table if exists courses;
drop table if exists attends;
drop table if exists teaches;
------------------------------------------------------------------------


-- Create schema
------------------------------------------------------------------------
create table students (
    id int not null,
    fname varchar(20),
    lname varchar(100),
    country char(2),
    primary key (id),
    check (country like '__')
);

create table lecturers (
    staff_no int not null,
    fname varchar(32),
    lname varchar(32),
    office char(8),
    primary key (staff_no)
);

create table courses (
    dept varchar(10) not null,
    num int not null,
    description text,
    coord_no int,
    rep_id int,
    primary key(dept, num),
    foreign key (coord_no) references lecturers (staff_no),
    foreign key (rep_id) references students (id)
);

create table attends (
    student_id int not null,
    course_dept varchar(10) not null,
    course_num int not null,
    semester char(2),
    mark char(2),
    primary key (student_id, course_dept, course_num),
    foreign key (student_id) references students (id),
    foreign key (course_dept, course_num) references courses (dept, num)
);

create table teaches (
    course_dept varchar(10) not null,
    course_num int not null,
    staff_no int not null,
    semester char(2) not null,
    primary key (course_dept, course_num, staff_no, semester),
    foreign key (course_dept, course_num) references courses (dept, num),
    foreign key (staff_no) references lecturers (staff_no)
);
------------------------------------------------------------------------


-- Add data to database
------------------------------------------------------------------------

-- Adding students
insert into students values (1667, 'John', 'Doe', 'AU');
insert into students values (1668, 'Jane', 'Smith', 'NZ');
insert into students values (1669, 'Thomas', 'T-Rex', 'NZ');

-- Adding lecturers
insert into lecturers values (1234, 'Andrew', 'Meads', '303.520');
insert into lecturers values (5678, 'Yu-Cheng', 'Tu', '303.516');

-- Adding courses
insert into courses values ('COMPSCI', 718, 'Java course', 5678, 1669); -- CS718's course co-ordinator is Yu-Cheng, and the student rep is Thomas.
insert into courses values ('COMPSCI', 719, 'Web course', 5678, 1668); -- CS719's course co-ordinator is Yu-Cheng, and the student rep is Jane.

-- Specifying that students attend courses
insert into attends values (1667, 'COMPSCI', 718, 'S1', 'A+'); -- John attended CS718 in S1, and got an A+.
insert into attends values (1667, 'COMPSCI', 719, 'S2', null); -- John attended CS719 in S2, and we don't know his mark yet.

-- Specifying that lecturers teach courses
insert into teaches values ('COMPSCI', 718, 1234, 'S1'); -- CS718 is taught by Andrew in S1.
insert into teaches values ('COMPSCI', 718, 5678, 'S1'); -- CS718 is also taught by Yu-Cheng in S1.
insert into teaches values ('COMPSCI', 719, 1234, 'S1'); -- CS719 is taught by Andrew in S1.
insert into teaches values ('COMPSCI', 719, 1234, 'S2'); -- CS719 is taught by Andrew in S2.

-- Errors!
insert into students values (1670, 'Not', 'Exist', 'Q'); -- Won't work because "Q" is not a valid country code (they must have 2 characters).
-- insert into attends values (9999, 'COMPSCI', 718, 'S2', 'D-'); -- Won't work because 9999 is not an id of an existing student.
-- insert into teaches values ('CUMPSCI', 718, 1234, 'SS'); -- Won't work because "CUMPSCI" is not a valid course department.
------------------------------------------------------------------------