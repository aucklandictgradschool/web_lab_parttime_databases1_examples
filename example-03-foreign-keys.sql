drop table if exists students;
drop table if exists lecturers;
drop table if exists courses;
drop table if exists attends;
drop table if exists teaches;

create table students (
    id int not null,
    fname varchar(20),
    lname varchar(100),
    country char(2),
    primary key (id),
    check (country like '__')
);

create table lecturers (
    staff_no int not null,
    fname varchar(32),
    lname varchar(32),
    office char(8),
    primary key (staff_no)
);

create table courses (
    dept varchar(10) not null,
    num int not null,
    description text,
    coord_no int,
    rep_id int,
    primary key(dept, num),
    foreign key (coord_no) references lecturers (staff_no), -- the course co-ordinator must be an existing lecturer.
    foreign key (rep_id) references students (id) -- the student rep must be an existing student.
);

create table attends (
    student_id int not null,
    course_dept varchar(10) not null,
    course_num int not null,
    semester char(2),
    mark char(2),
    primary key (student_id, course_dept, course_num), -- each student can only be enrolled in a given subject once.
    foreign key (student_id) references students (id), -- only existing students can attend courses.
    foreign key (course_dept, course_num) references courses (dept, num) -- students can only attend existing courses.
);

create table teaches (
    course_dept varchar(10) not null,
    course_num int not null,
    staff_no int not null,
    semester char(2) not null,
    primary key (course_dept, course_num, staff_no, semester), -- each lecturer can only teach each subject once per semester.
    foreign key (course_dept, course_num) references courses (dept, num), -- only existing courses can be taught.
    foreign key (staff_no) references lecturers (staff_no) -- only existing staff may teach courses.
);